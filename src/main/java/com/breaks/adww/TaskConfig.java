package com.breaks.adww;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

@ConfigurationProperties(prefix = "taskconfig")
@Configuration
public class TaskConfig implements InitializingBean {

    private int segmentSize;

    private String sourceFileDir;

    private String destFileDir;
    
    private long imageMinSize;
    
    private String imagePixelSizeFilter;
    
    public int getSegmentSize() {
        return segmentSize;
    }

    public void setSegmentSize(int segmentSize) {
        this.segmentSize = segmentSize;
    }

    public String getSourceFileDir() {
        return sourceFileDir;
    }

    public void setSourceFileDir(String sourceFileDir) {
        this.sourceFileDir = sourceFileDir;
    }

    public String getDestFileDir() {
        return destFileDir;
    }

    public void setDestFileDir(String destFileDir) {
        this.destFileDir = destFileDir;
    }

    public long getImageMinSize() {
        return imageMinSize;
    }

    public void setImageMinSize(long imageMinSize) {
        this.imageMinSize = imageMinSize;
    }

    public String getImagePixelSizeFilter() {
        return imagePixelSizeFilter;
    }

    public void setImagePixelSizeFilter(String imagePixelSizeFilter) {
        this.imagePixelSizeFilter = imagePixelSizeFilter;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        Properties properties = new Properties();
        try {
            // 使用ClassLoader加载properties配置文件生成对应的输入流
            InputStream in = new BufferedInputStream(new FileInputStream(new File("").getAbsolutePath() + File.separator + "config.properties"));
            // 使用properties对象加载输入流
            properties.load(in);
            //获取key对应的value值
            sourceFileDir = properties.getProperty("sourceFileDir", sourceFileDir);
            if (!StringUtils.isEmpty(sourceFileDir)) {
                String username = System.getProperty("user.name");
                sourceFileDir = sourceFileDir.replace("{username}", username);
            }
            destFileDir = properties.getProperty("destFileDir", destFileDir);
            segmentSize = Integer.parseInt(properties.getProperty("segmentSize", String.valueOf(segmentSize)));
            imageMinSize = Long.parseLong(properties.getProperty("imageMinSize", String.valueOf(imageMinSize)));
            imagePixelSizeFilter = properties.getProperty("imagePixelSizeFilter", imagePixelSizeFilter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int[][] getImagePixelSizeFilterArray() {
        String ipsfTemp = imagePixelSizeFilter;
        if (!StringUtils.isEmpty(ipsfTemp)) {
            String[] split = ipsfTemp.split(",");
            int[][] filters = new int[split.length][];
            for (int i = 0; i < split.length; i++) {
                String[] split1 = split[i].split("\\*");
                filters[i] = new int[]{Integer.parseInt(split1[0]), Integer.parseInt(split1[1])};
            }
            return filters;
        }
        return null;
    }

}
