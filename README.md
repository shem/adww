# adww（auto download windows10 wallpaper）

#### 介绍
用于自动拷贝windows10系统c盘中的锁屏壁纸。

每次打开windows10的时候，看到锁屏壁纸，都觉得很好看，想把它们都拷贝下来，作为桌面壁纸，但每次手动拷贝又太过麻烦，而且锁屏壁纸过一段时间就会被换掉，原文件就直接被清除了，所以就想写个程序自动拷贝，这就是这个项目的来源。

#### 安装教程

将jar执行包安装为windows服务的方法有JavaService、winsw、nssm三种方法，具体教程可参考 https://blog.csdn.net/qq_33443402/article/details/80168877

本人采用的是winsw，因为它是一个开源项目，而且至今还在更新，地址是https://github.com/winsw/winsw，不过这也只是个工具而已，用哪一种没什么大的关系，看自己喜欢了。

