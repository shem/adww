	1、配置好JAVA_HOME环境变量，以及adww.xml，config.properties以后，使用adww.exe install在cmd中进行服务安装，adww.xml需要配置端口，默认端口为8080。
	2、启动服务后，在logs/adww.out.log文件中查看是否成功运行，如果有报错，可能就是配置过程中有问题，停止服务后，重新配置，然后再启动；
		如果adww.xml配置错误，那么需要卸载服务后，重新进行服务安装。